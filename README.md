# README #

Bulding Escape

### What is this repository for? ###

* This is a Unreal engine project for an escape room puzzle
* Produced with Unreal engine 4.16
* Other Unreal versions are untested
* Product version 0.9 BETA

### How do I get set up? ###

* Contains all neccessary files but must rebuild intermediate files to play
* Open project in Unreal engine and rebuild project
* Compile into executable and play :)

### Known Issues ###

* Collision mesh for doors may not be set in game when project is opened from repository

